<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri='http://java.sun.com/jsp/jstl/core' %>

<!DOCTYPE html>
<title>Admin Dashboard | mRecharge</title>
<t:baseAdminTag>
    <jsp:body>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    ADMIN DASHBOARD
                    <small>...</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>


            <section class="content">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="box">
                            <div class="box-header">

                                <c:if test="${not empty emsg}">
                                    <div class="text text-danger text-center">
                                        <h5> ${emsg }</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty msg}">
                                    <div class="text text-success text-center">
                                        <h5> ${msg}</h5>
                                    </div>
                                </c:if>


                                <h3 class="box-title">List of vendors</h3>

                                <div class="box-tools">
                                    <div>
                                        <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Add New Vendor</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sno</th>
                                        <th>Name</th>
                                        <th>Creation Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    <c:forEach var="vendor" items="${vendors}" varStatus="idx">
                                        <tr>
                                            <td>${idx.index + 1}</td>
                                            <td>${vendor.name}</td>
                                            <td>${vendor.registrationDate}</td>
                                            <td>${vendor.status}</td>
                                            <td>
                                                <a href="/admin/services/view/${vendor.id}">
                                                    <button class="btn btn-success btn-xs">view services</button>
                                                </a>
                                                <button class="btn btn-warning btn-xs disabled">deactivate</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add a vendor</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/create_vendor" method="post">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Enter vendor name" name="vendor_name" required>
                            </div>
                            <div class="row" align="center">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Create Vendor</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




    </jsp:body>
</t:baseAdminTag>
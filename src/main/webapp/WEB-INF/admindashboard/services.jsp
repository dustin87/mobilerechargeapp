<%--<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri='http://java.sun.com/jsp/jstl/core' %>--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<title>Admin Dashboard | mRecharge</title>
<t:baseAdminTag>
    <jsp:body>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    ADMIN DASHBOARD
                    <small>...</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>


            <section class="content">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="box">
                            <div class="box-header">

                                <c:if test="${not empty emsg}">
                                    <div class="text text-danger text-center">
                                        <h5> ${emsg }</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty msg}">
                                    <div class="text text-success text-center">
                                        <h5> ${msg}</h5>
                                    </div>
                                </c:if>


                                <h3 class="box-title">ALL SERVICES</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sno</th>
                                        <th>Name</th>
                                        <th>Vendor</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    <c:forEach var="service" items="${services}" varStatus="idx">
                                        <tr>
                                            <td>${idx.index + 1}</td>
                                            <td>${service.name}</td>
                                            <td>${service.vendor.name}</td>
                                            <td>N${service.amount}</td>
                                            <td>
                                                <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#editService" onclick="fetchVendorService('${service.id}')">Edit</button>
                                                <button class="btn btn-warning btn-xs disabled" >Deactivate</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>


        <!-- EDIT SERVICE MODAL -->
        <div class="modal fade" id="editService" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="editTitle"></h4>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/edit_vendor_service" method="post">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Enter Service name" name="sName" required id="vsName">
                            </div>

                            <div class="form-group has-feedback">
                                <input type="number" class="form-control" placeholder="Enter Service amount" name="sAmount" id="vsAmount" required>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="hidden" class="form-control"  name="vsId" id="vsId">
                            </div>





                            <div class="row" align="center">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Update Service</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




        <script>
            function fetchVendorService(id){
                $.ajax({
                    url: "/admin/vendor_service/get/"+id,
                    type: 'GET',
                    dataType: 'json',
                    /*data: branch,*/
                    success: function (result) {
                        console.log(result);
                        $('#vsName').val(result.object.name);
                        $('#vsAmount').val(result.object.amount);
                        $('#vsId').val(result.object.id);
                    },

                    error: function (error) {
                        alert("Unable to fetch service for edit purposes");
                    }
                });



            }
        </script>

    </jsp:body>
</t:baseAdminTag>
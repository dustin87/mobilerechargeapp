<%--<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri='http://java.sun.com/jsp/jstl/core' %>--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<title>Admin Dashboard | mRecharge</title>
<t:baseUsertag>
    <jsp:body>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>User Dashboard</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>


            <section class="content">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="box">
                            <div class="box-header">

                                <c:if test="${not empty emsg}">
                                    <div class="text text-danger text-center">
                                        <h5> ${emsg }</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty msg}">
                                    <div class="text text-success text-center">
                                        <h5> ${msg}</h5>
                                    </div>
                                </c:if>


                                <h3 class="box-title">Enjoy all available services</h3>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sno</th>
                                        <th>Name</th>
                                        <th>Vendor</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    <c:forEach var="service" items="${services}" varStatus="idx">
                                        <tr>
                                            <td>${idx.index + 1}</td>
                                            <td>${service.name}</td>
                                            <td><span class="label label-warning">${service.vendor.name}</span></td>
                                            <td><span class="label label-primary">N${service.amount}</span></td>
                                            <td>
                                                <button class="btn btn-success btn-xs" onclick="onRecharge('${service.id}','${service.amount}', '${service.name}')">Recharge</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>


        <!-- CREATE SERVICE MODAL -->
        <div class="modal fade" id="addService" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add a service for ${vendor.name}</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/create_vendor_service/${vendor.id}" method="post">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Enter Service name" name="sName" required>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="number" class="form-control" placeholder="Enter Service amount" name="sAmount" required>
                            </div>





                            <div class="row" align="center">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Create Service</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function onRecharge(id, amount, plan){
                if(confirm("Great Choice!! Are you sure you want to go with this beautiful N" + amount + " " + plan + " plan?")){
                    setTimeout(function() {
                        alert("Your recharge was successful");
                        window.location.href = '/user_dashboard/payment_successful?vendor_service_id='+id;
                    }, 1000);
                }
            }
        </script>


    </jsp:body>
</t:baseUsertag>
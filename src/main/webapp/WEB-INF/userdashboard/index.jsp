<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri='http://java.sun.com/jsp/jstl/core' %>

<!DOCTYPE html>
<title>USER DASHBOARD | mRecharge</title>
<t:baseUsertag>
    <jsp:body>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>User Dashboard</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>


            <section class="content">
                <div class="row">
                    <c:forEach var="vendor" items="${vendors}" varStatus="idx">
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>${fn:toUpperCase(vendor.name)}</h3>

                                    <p>New vendor *</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="/user_dashboard/vendor_services?vendorid=${vendor.id}" class="small-box-footer">View services <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </section>
        </div>
        </div>
    </jsp:body>
</t:baseUsertag>
<%--<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri='http://java.sun.com/jsp/jstl/core' %>--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<title>Admin Dashboard | mRecharge</title>
<t:baseUsertag>
    <jsp:body>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>User Dashboard</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>


            <section class="content">
                <div class="row">
                    <div class="col-xs-10">
                        <div class="box">
                            <div class="box-header">

                                <c:if test="${not empty emsg}">
                                    <div class="text text-danger text-center">
                                        <h5> ${emsg }</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty msg}">
                                    <div class="text text-success text-center">
                                        <h5> ${msg}</h5>
                                    </div>
                                </c:if>


                                <h3 class="box-title">Transaction history</h3>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sno</th>
                                        <th>Transaction Id</th>
                                        <th>Total Amount</th>
                                        <th>Convinience Charge</th>
                                        <th>Date</th>
                                        <th>Payment status</th>
                                    </tr>
                                    <c:forEach var="trans" items="${transactions}" varStatus="idx">
                                        <tr>
                                            <td>${idx.index + 1}</td>
                                            <td>${trans.transactionId}</td>
                                            <td><span class="label label-primary">N${trans.totalAmount}</span></td>

                                            <td>${trans.convinienceCharge}</td>
                                            <td>${trans.regDate}</td>
                                            <td>${trans.paymentStatus}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>





    </jsp:body>
</t:baseUsertag>
<%--<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri='http://java.sun.com/jsp/jstl/core' %>--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<title>User Dashboard | mRecharge</title>
<t:baseUsertag>
    <jsp:body>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>User Dashboard</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>


            <section class="content">
                <div class="row">
                    <div class="col-xs-10">
                        <div class="box">
                            <div class="box-header">

                                <c:if test="${not empty emsg}">
                                    <div class="text text-danger text-center">
                                        <h5> ${emsg }</h5>
                                    </div>
                                </c:if>
                                <c:if test="${not empty msg}">
                                    <div class="text text-success text-center">
                                        <h5> ${msg}</h5>
                                    </div>
                                </c:if>


                                <h3 class="box-title">Tickets</h3>
                                <div class="box-tools">
                                    <div>
                                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#createTicket">NEW TICKET*</button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sno</th>
                                        <th>Ticket Id</th>
                                        <th>Subject</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    <c:forEach var="ticket" items="${tickets}" varStatus="idx">
                                        <tr>
                                            <td>${idx.index + 1}</td>
                                            <td>${ticket.ticketId}</td>
                                            <td>${ticket.subject}</td>
                                            <td><span class="label label-warning">${ticket.tickeStatus}</span></td>
                                            <td>${ticket.creationDate}</td>
                                            <td>
                                                <button class="btn btn-success btn-xs" data-toggle="modal"
                                                        data-target="#viewTicket" onclick="fetchTicketDetails('${ticket.id}')">Details</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>


        <!-- CREATE TICKET MODAL -->
        <div class="modal fade" id="createTicket" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create a ticket</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/user_dashboard/create_ticket" method="post">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Enter Subject of Ticket" maxlength="20" name="subject" required>
                            </div>

                            <div class="form-group has-feedback">
                                <label>Explain the problem</label>
                                <textarea class="form-control" name="ticketissue" maxlength="2000" rows="10" required></textarea>
                            </div>





                            <div class="row" align="center">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Create Ticket</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- TICKET DETAILS MODAL -->
        <div class="modal fade" id="viewTicket" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">TICKET DETAILS</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" method="post">
                            <div class="form-group has-feedback">
                                <label>Subject</label>
                                <p id="theSubject"></p>
                            </div>

                            <div class="form-group has-feedback">
                                <label>The problem</label>
                                <p id="theIssue"></p>
                            </div>

                            <div class="form-group has-feedback">
                                <label>Response</label>
                                <p id="theResponse"></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function fetchTicketDetails(id){
                $.ajax({
                    url: "/get_ticket/"+id,
                    type: 'GET',
                    dataType: 'json',
                    /*data: branch,*/
                    success: function (result) {
                        console.log(result);
                        $('#theSubject').html(result.object.subject);
                        $('#theIssue').html(result.object.issueRaised);
                        if(result.object.response == '' || result.object.response == null){
                            $('#theResponse').html('NO RESPONSE')
                        }else{
                            $('#theResponse').html(result.object.response)
                        }
                    },

                    error: function (error) {
                        alert("Unable to ticket");
                    }
                });
            }
        </script>





    </jsp:body>
</t:baseUsertag>
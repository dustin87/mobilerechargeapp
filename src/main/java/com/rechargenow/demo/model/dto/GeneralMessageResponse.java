package com.rechargenow.demo.model.dto;

import java.io.Serializable;

public class GeneralMessageResponse {
    private String status;
    private Serializable object;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Serializable getObject() {
        return object;
    }

    public void setObject(Serializable object) {
        this.object = object;
    }
}

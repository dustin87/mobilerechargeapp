package com.rechargenow.demo.model;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by David on 7/25/2019.
 */
@Entity
public class Ticket {
    public enum TicketStatus{OPEN, CLOSED}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String ticketId;
    private String subject;

    @Column(length = 2000)
    private String issueRaised;
    @Column(length = 2000)
    private String response;
    @ManyToOne
    @JoinColumn(name = "portalUser", referencedColumnName = "id")
    private PortalUser portalUser;

    @ManyToOne
    @JoinColumn(name = "adminstrator", referencedColumnName = "id")
    private Adminstrator adminstrator;

    private TicketStatus tickeStatus;
    private LocalDateTime creationDate;
    private LocalDateTime resolutionDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIssueRaised() {
        return issueRaised;
    }

    public void setIssueRaised(String issueRaised) {
        this.issueRaised = issueRaised;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public PortalUser getPortalUser() {
        return portalUser;
    }

    public void setPortalUser(PortalUser portalUser) {
        this.portalUser = portalUser;
    }

    public Adminstrator getAdminstrator() {
        return adminstrator;
    }

    public void setAdminstrator(Adminstrator adminstrator) {
        this.adminstrator = adminstrator;
    }

    public TicketStatus getTickeStatus() {
        return tickeStatus;
    }

    public void setTickeStatus(TicketStatus tickeStatus) {
        this.tickeStatus = tickeStatus;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getResolutionDate() {
        return resolutionDate;
    }

    public void setResolutionDate(LocalDateTime resolutionDate) {
        this.resolutionDate = resolutionDate;
    }
}

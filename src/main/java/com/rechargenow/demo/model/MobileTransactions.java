package com.rechargenow.demo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class MobileTransactions implements Serializable {
    private static final long serialVersionUID = -4358934782506546691L;

    public enum PaymentStatus{INITIATED, PAID, NOT_PAID, UNVERIFIED}
    public enum ValueStatus{HAS_VALUE, NO_VALUE}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String transactionId;
    private Double totalAmount;
    private Double convinienceCharge;
    private LocalDateTime regDate;
    private PaymentStatus paymentStatus;
    private String transactionDescription;
    private String paymentGateway;
    private String pspReferenceId;
    @Column(length = 2000)
    private String pspPayload;
    private ValueStatus valueStatus;
    @ManyToOne
    @JoinColumn(name = "portalUser", referencedColumnName = "id", nullable = false)
    private PortalUser portalUser;
    @ManyToOne
    @JoinColumn(name = "vendorMobileService", referencedColumnName = "id", nullable = false)
    private VendorMobileService vendorMobileService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getConvinienceCharge() {
        return convinienceCharge;
    }

    public void setConvinienceCharge(Double convinienceCharge) {
        this.convinienceCharge = convinienceCharge;
    }

    public LocalDateTime getRegDate() {
        return regDate;
    }

    public void setRegDate(LocalDateTime regDate) {
        this.regDate = regDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getPspReferenceId() {
        return pspReferenceId;
    }

    public void setPspReferenceId(String pspReferenceId) {
        this.pspReferenceId = pspReferenceId;
    }

    public String getPspPayload() {
        return pspPayload;
    }

    public void setPspPayload(String pspPayload) {
        this.pspPayload = pspPayload;
    }

    public ValueStatus getValueStatus() {
        return valueStatus;
    }

    public void setValueStatus(ValueStatus valueStatus) {
        this.valueStatus = valueStatus;
    }

    public PortalUser getPortalUser() {
        return portalUser;
    }

    public void setPortalUser(PortalUser portalUser) {
        this.portalUser = portalUser;
    }

    public VendorMobileService getVendorMobileService() {
        return vendorMobileService;
    }

    public void setVendorMobileService(VendorMobileService vendorMobileService) {
        this.vendorMobileService = vendorMobileService;
    }
}

package com.rechargenow.demo.model;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Vendor implements Serializable {
    private static final long serialVersionUID = -4358934782506546691L;

    public enum AccountStatus{ACTIVE, INACTIVE, SUSPENDED, DELETED}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private LocalDateTime registrationDate;
    private AccountStatus status;
    @ManyToOne
    @JoinColumn(name = "administrator", referencedColumnName = "id")
    private Adminstrator adminstrator;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public Adminstrator getAdminstrator() {
        return adminstrator;
    }

    public void setAdminstrator(Adminstrator adminstrator) {
        this.adminstrator = adminstrator;
    }
}

package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by David on 7/25/2019.
 */
public interface TicketDao extends JpaRepository<Ticket, Long> {
    List<Ticket> findAllByPortalUser(PortalUser portalUser);
    List<Ticket> findAllByTickeStatus(Ticket.TicketStatus status);
}

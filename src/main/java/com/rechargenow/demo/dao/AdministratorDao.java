package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.Adminstrator;
import com.rechargenow.demo.model.Useraccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdministratorDao extends JpaRepository<Adminstrator, Long> {
    Adminstrator findByUseraccount(Useraccount account);
}

package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Useraccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortalUserDao extends JpaRepository<PortalUser, Long> {
    PortalUser findByUseraccount(Useraccount useraccount);
}

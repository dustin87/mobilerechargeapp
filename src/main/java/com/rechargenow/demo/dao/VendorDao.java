package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VendorDao extends JpaRepository<Vendor, Long> {
    List<Vendor> findAllByName(String name);
}

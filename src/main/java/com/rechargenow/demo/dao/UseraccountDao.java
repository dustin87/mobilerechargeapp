package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.Useraccount;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UseraccountDao extends JpaRepository<Useraccount, Long> {
    Useraccount findByEmail(String email);
    Useraccount findByEmailAndPassword(String email, String password);
}

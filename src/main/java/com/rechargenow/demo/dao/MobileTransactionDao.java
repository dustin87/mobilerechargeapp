package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.MobileTransactions;
import com.rechargenow.demo.model.PortalUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MobileTransactionDao extends JpaRepository<MobileTransactions, Long> {
    List<MobileTransactions> findAllByPortalUser(PortalUser pUser);
}

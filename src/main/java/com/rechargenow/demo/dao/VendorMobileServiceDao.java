package com.rechargenow.demo.dao;

import com.rechargenow.demo.model.Vendor;
import com.rechargenow.demo.model.VendorMobileService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VendorMobileServiceDao extends JpaRepository<VendorMobileService, Long> {
    List<VendorMobileService> findAllByVendor(Vendor vendor);
    List<VendorMobileService> findAllByName(String name);
}

package com.rechargenow.demo.controller;

import com.google.common.base.Strings;
import com.rechargenow.demo.model.*;
import com.rechargenow.demo.service.account.SessionManager;
import com.rechargenow.demo.service.jpa.*;
import com.rechargenow.demo.util.DateTimeUtilities;
import com.rechargenow.demo.util.GeneralUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * FOR ALL USERDASHBOARD ACTIVITIES
 */


@Controller
public class UserDashboardController {
    @Autowired SessionManager sessionManager;
    @Autowired VendorService vendorService;
    @Autowired AdministratorService administratorService;
    @Autowired DateTimeUtilities dateTimeUtilities;
    @Autowired VendorMobileServiceService vendorMobileServiceService;
    @Autowired PortalUserSerivice portalUserSerivice;
    @Autowired MobileTransactionsService mobileTransactionsService;
    @Autowired TicketService ticketService;


    @GetMapping("/user_dashboard")
    private String adminDashboard(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);


        //get all vendors
        model.addAttribute("vendors", vendorService.getAll());


        return "userdashboard/index";
    }


    @GetMapping("/user_dashboard/vendor_services")
    private String viewVendorServices(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);

        if(Strings.isNullOrEmpty(req.getParameter("vendorid"))){
            return "redirect:/user_dashboard";
        }

        //Fetch Vendor and Services
        Vendor vendor = vendorService.getOne(Long.valueOf(req.getParameter("vendorid")));
        if(vendor == null)
            return "redirect:/user_dashboard";


        model.addAttribute("services", vendorMobileServiceService.findAllBy(vendor));
        model.addAttribute("vendor", vendor);

        return "userdashboard/vendorServices";
    }

    @GetMapping("/user_dashboard/services/all")
    private String viewAllServices(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);


        model.addAttribute("services", vendorMobileServiceService.getAll());

        return "userdashboard/services";
    }

    @GetMapping("/user_dashboard/payment_successful")
    private String afterSuccessfulRecharge(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);

        if(Strings.isNullOrEmpty(req.getParameter("vendor_service_id"))){
            return "redirect:/user_dashboard";
        }

        //Fetch VendorService
        VendorMobileService vService = vendorMobileServiceService.getOne(Long.valueOf(req.getParameter("vendor_service_id")));
        if(vService == null)
            return "redirect:/user_dashboard";


        //Create Transaction
        MobileTransactions mTransaction = new MobileTransactions();
        mTransaction.setConvinienceCharge(0.0);
        mTransaction.setTransactionId(GeneralUtil.generateRandomString(7));
        mTransaction.setPaymentGateway("DEFAULT");
        mTransaction.setPaymentStatus(MobileTransactions.PaymentStatus.PAID);
        mTransaction.setPortalUser(pUser);
        mTransaction.setPspPayload("");
        mTransaction.setPspReferenceId("DEFAULT");
        mTransaction.setRegDate(dateTimeUtilities.getDateTime());
        mTransaction.setTotalAmount(vService.getAmount());
        mTransaction.setTransactionDescription("Payment for "+vService.getName());
        mTransaction.setValueStatus(MobileTransactions.ValueStatus.HAS_VALUE);
        mTransaction.setVendorMobileService(vService);
        mobileTransactionsService.save(mTransaction);


        return "redirect:/user_dashboard";
    }

    @GetMapping("/user_dashboard/transactions")
    private String transactionHistory(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);


        model.addAttribute("transactions", mobileTransactionsService.findAllBy(pUser));

        return "userdashboard/transactions";
    }



    /**************************TICKETS****************************/
    @GetMapping("/user_dashboard/tickets")
    private String viewAlltickets(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);


        model.addAttribute("tickets", ticketService.findAllBy(pUser));

        return "userdashboard/ticket_all";
    }

    @PostMapping("/user_dashboard/create_ticket")
    private String createTicket(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserPortalUser(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        PortalUser pUser = portalUserSerivice.findBy(acct);

        //Create Ticket
        Ticket ticket = new Ticket();
        ticket.setCreationDate(dateTimeUtilities.getDateTime());
        ticket.setIssueRaised(req.getParameter("ticketissue"));
        ticket.setPortalUser(pUser);
        ticket.setSubject(req.getParameter("subject"));
        ticket.setTickeStatus(Ticket.TicketStatus.OPEN);
        ticket.setTicketId(GeneralUtil.generateRandomString(10));
        ticketService.save(ticket);

        rd.addFlashAttribute("msg", "A ticket with id "+ ticket.getTicketId() + " has been created and forwarded to the Administrator");
        return "redirect:/user_dashboard/tickets";
    }

}

package com.rechargenow.demo.controller;

import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Useraccount;
import com.rechargenow.demo.service.account.SessionManager;
import com.rechargenow.demo.service.jpa.PortalUserSerivice;
import com.rechargenow.demo.service.jpa.RoleService;
import com.rechargenow.demo.service.jpa.UseraccountService;
import com.rechargenow.demo.util.DateTimeUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AccountController
{
    @Autowired SessionManager sessionManager;
    @Autowired UseraccountService useraccountService;
    @Autowired DateTimeUtilities dateTimeUtilities;
    @Autowired RoleService roleService;
    @Autowired PortalUserSerivice portalUserSerivice;

    @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
    public String login(HttpServletRequest req, RedirectAttributes rd)
    {

        if(req.getMethod().equals(RequestMethod.POST.name()))
        {
            //find account using email and password
            Useraccount useraccount = useraccountService.findBy(req.getParameter("email").trim(), req.getParameter("password").trim());
            if(useraccount == null){
                rd.addFlashAttribute("emsg", "Invalid username or password");
                return "redirect:/login";
            }

            //validate accdt
            if(!useraccount.getAccountStatus().equals(Useraccount.AccountStatus.ACTIVE)){
                rd.addFlashAttribute("emsg", "Account Inactive. Kindly contact support");
                return "redirect:/login";
            }

            //create session
            sessionManager.createSession(req, useraccount.getEmail());
            return "redirect:/login";
        }




        //if the user is already loggedin, redirect to dashboard
        if(sessionManager.isUserSessionActive(req))
        {
            if(sessionManager.isUserAdmin(req)){
                return "redirect:/admin";
            }
            if(sessionManager.isUserPortalUser(req))
            {
                return "redirect:/user_dashboard";
            }
        }




        sessionManager.destroySession(req);
        return "login";
    }

    @RequestMapping(value = "/register", method = {RequestMethod.POST, RequestMethod.GET})
    public String register(HttpServletRequest req, Model model, RedirectAttributes rd) {


        if(req.getMethod().equals(RequestMethod.POST.name())) {
            //validate email
            if(useraccountService.findByEmail(req.getParameter("email")) != null){
                rd.addFlashAttribute("emsg", "This Email already exist. Please try another");
                return "redirect:/register";
            }

            //Create user
            Useraccount acct = new Useraccount();
            acct.setEmail(req.getParameter("email"));
            acct.setPhone(req.getParameter("phone"));
            acct.setPassword(req.getParameter("password"));
            acct.setAccountStatus(Useraccount.AccountStatus.ACTIVE);
            acct.setRegistrationDate(dateTimeUtilities.getDateTime());
            acct.setRole(roleService.findByName("PORTAL_USER_ROLE"));
            acct = useraccountService.save(acct);

            PortalUser user = new PortalUser();
            user.setFirstName(req.getParameter("firstName"));
            user.setLastName(req.getParameter("lastName"));
            user.setUseraccount(acct);
            portalUserSerivice.save(user);

            rd.addFlashAttribute("emsg", "Registration Successful. Kindly login!!");
            return "redirect:/login";
        }

        return "register";

    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest req){
        sessionManager.destroySession(req);

        return "redirect:/login";
    }

}

package com.rechargenow.demo.controller;

import com.rechargenow.demo.model.Ticket;
import com.rechargenow.demo.model.VendorMobileService;
import com.rechargenow.demo.model.dto.GeneralMessageResponse;
import com.rechargenow.demo.service.account.SessionManager;
import com.rechargenow.demo.service.jpa.AdministratorService;
import com.rechargenow.demo.service.jpa.TicketService;
import com.rechargenow.demo.service.jpa.VendorMobileServiceService;
import com.rechargenow.demo.service.jpa.VendorService;
import com.rechargenow.demo.util.DateTimeUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@Controller
public class RestController {

    @Autowired SessionManager sessionManager;
    @Autowired VendorService vendorService;
    @Autowired AdministratorService administratorService;
    @Autowired DateTimeUtilities dateTimeUtilities;
    @Autowired VendorMobileServiceService vendorMobileServiceService;
    @Autowired TicketService ticketService;

    @ResponseBody
    @GetMapping("/admin/vendor_service/get/{vendor_service_id}")
    public ResponseEntity<?> getOneVendorService(HttpServletRequest req, @PathVariable("vendor_service_id") Long vsId)
    {
        VendorMobileService vms = vendorMobileServiceService.getOne(vsId);

        GeneralMessageResponse resp = new GeneralMessageResponse();
        resp.setStatus("SUCCESS");
        resp.setObject((Serializable) vms);

        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/get_ticket/{ticketid}")
    public ResponseEntity<?> getTicketDetails(HttpServletRequest req, @PathVariable("ticketid") Long tId)
    {
        Ticket ticket = ticketService.getOne(tId);

        GeneralMessageResponse resp = new GeneralMessageResponse();
        resp.setStatus("SUCCESS");
        resp.setObject((Serializable) ticket);

        return new ResponseEntity<>(resp, HttpStatus.OK);
    }
}

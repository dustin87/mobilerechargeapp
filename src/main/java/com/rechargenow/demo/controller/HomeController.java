package com.rechargenow.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController
{
    @GetMapping("/")
    public String home(HttpServletRequest req)
    {
        return "redirect:/user_dashboard";
    }

}

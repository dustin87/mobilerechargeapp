package com.rechargenow.demo.controller;

import com.google.common.base.Strings;
import com.rechargenow.demo.dao.AdministratorDao;
import com.rechargenow.demo.model.*;
import com.rechargenow.demo.model.dto.GeneralMessageResponse;
import com.rechargenow.demo.service.account.SessionManager;
import com.rechargenow.demo.service.jpa.*;
import com.rechargenow.demo.util.DateTimeUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * FOR ALL ADMIN ACTIVITIES
 */


@Controller
public class AdminController {
    @Autowired SessionManager sessionManager;
    @Autowired VendorService vendorService;
    @Autowired AdministratorService administratorService;
    @Autowired DateTimeUtilities dateTimeUtilities;
    @Autowired VendorMobileServiceService vendorMobileServiceService;
    @Autowired MobileTransactionsService mobileTransactionsService;
    @Autowired TicketService ticketService;


    @GetMapping("/admin")
    public String adminDashboard(HttpServletRequest req, Model model)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }

        //fetch all vendors
        model.addAttribute("vendors", vendorService.getAll());

        return "admindashboard/index";
    }

    @PostMapping("/admin/create_vendor")
    public String createVendor(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);



        if(Strings.isNullOrEmpty(req.getParameter("vendor_name"))){
            return "redirect:/admin";
        }

        //Create new vendor
        Vendor vendor = new Vendor();
        vendor.setName(req.getParameter("vendor_name"));
        vendor.setRegistrationDate(dateTimeUtilities.getDateTime());
        vendor.setAdminstrator(adminstrator);
        vendor.setStatus(Vendor.AccountStatus.ACTIVE);
        vendorService.save(vendor);


        rd.addFlashAttribute("msg", "Vendor Created Successfully");
        return "redirect:/admin";
    }


    @GetMapping("/admin/services/view_all")
    public String viewAllServices(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);


        //Fetch ALL Services
        model.addAttribute("services", vendorMobileServiceService.getAll());

        return "admindashboard/services";
    }


    @GetMapping("/admin/services/view/{vendorid}")
    public String veiwVendorServices(HttpServletRequest req, Model model, RedirectAttributes rd, @PathVariable("vendorid") Long vId)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);


        //Fetch Vendor Services
        Vendor vendor = vendorService.getOne(vId);
        if(vendor == null)
            return "redirect:/admin";

        model.addAttribute("services", vendorMobileServiceService.findAllBy(vendor));
        model.addAttribute("vendor", vendor);

        return "admindashboard/vendorServices";
    }

    @PostMapping("/admin/create_vendor_service/{vendorid}")
    public String createVendorService(HttpServletRequest req, Model model, RedirectAttributes rd, @PathVariable("vendorid") Long vId)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);



        //Fetch Vendor
        Vendor vendor = vendorService.getOne(vId);
        if(vendor == null)
            return "redirect:/admin";


        VendorMobileService vService = new VendorMobileService();
        vService.setName(req.getParameter("sName"));
        vService.setAmount(Double.valueOf(req.getParameter("sAmount")));
        vService.setVendor(vendor);
        vendorMobileServiceService.save(vService);

        rd.addFlashAttribute("msg", "Service for "+ vendor.getName() + " created successfully");
        return "redirect:/admin/services/view/"+vendor.getId();
    }

    @PostMapping("/admin/edit_vendor_service")
    public String editVendorService(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);



        //Fetch VendorService
        if(Strings.isNullOrEmpty(req.getParameter("vsId"))){
            return "redirect:/admin";
        }
        VendorMobileService vService = vendorMobileServiceService.getOne(Long.valueOf(req.getParameter("vsId")));
        if(vService == null)
            return "redirect:/admin";


        vService.setName(req.getParameter("sName"));
        vService.setAmount(Double.valueOf(req.getParameter("sAmount")));
        vendorMobileServiceService.save(vService);

        rd.addFlashAttribute("msg", "Service Updated");
        return "redirect:/admin/services/view/"+vService.getVendor().getId();
    }


    @GetMapping("/admin/transactions/all")
    public String viewAllTransactions(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);


        //Fetch ALL transactions
        model.addAttribute("transactions", mobileTransactionsService.findAll());

        return "admindashboard/transactions";
    }


    /************************TICKETS***********************/
    @GetMapping("/admin/ticket/all")
    public String viewAllTickets(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);


        //Fetch ALL tickets
        model.addAttribute("tickets", ticketService.findAllBy(Ticket.TicketStatus.OPEN));

        return "admindashboard/ticket_all";
    }

    @PostMapping("/admin/ticket/respond")
    public String respondTtoTicket(HttpServletRequest req, Model model, RedirectAttributes rd)
    {
        //validation
        if(!sessionManager.isUserAdmin(req)){
            return "redirect:/login";
        }
        Useraccount acct = sessionManager.getCurrentAccount(req);
        Adminstrator adminstrator = administratorService.findOne(acct);


        //Fetch ticket
        if(Strings.isNullOrEmpty(req.getParameter("ticketId")))
            return "redirect:/admin";

        Ticket ticket = ticketService.getOne(Long.valueOf(req.getParameter("ticketId")));
        ticket.setResponse(req.getParameter("tResponse"));
        ticket.setTickeStatus(Ticket.TicketStatus.CLOSED);
        ticket.setAdminstrator(adminstrator);
        ticket.setResolutionDate(dateTimeUtilities.getDateTime());
        ticketService.update(ticket);


        rd.addFlashAttribute("msg", "Ticket Closed Successfully");
        return "redirect:/admin/ticket/all";
    }






}

package com.rechargenow.demo.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * Created by Oasis-Workstation2 on 1/9/2018.
 */
public class GeneralUtil
{
    private static final SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

    public static String generateRandomString(final int length) {
        Random r = new Random(); // perhaps make it a class variable so you don't make a new one every time
        char[] character = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0'};
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++) {
            char c = character[r.nextInt(35)];
            sb.append(c);
        }
        return sb.toString();
    }


    public static String reformatTimestamp(Timestamp timestamp)
    {
        String time = sdf3.format(timestamp);
        return time;

    }
}

package com.rechargenow.demo.service.account.impl;

import com.rechargenow.demo.model.Useraccount;
import com.rechargenow.demo.service.account.SessionManager;
import com.rechargenow.demo.service.jpa.UseraccountService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Service
@Transactional
public class SessionManagerImpl implements SessionManager {

    @Autowired UseraccountService userAccountService;
    private HttpSession userSession;

    @Override
    public void createSession(HttpServletRequest req, String email) {

        userSession = (HttpSession) req.getSession(true);
        userSession.setMaxInactiveInterval(3600 * 10);
        userSession.setAttribute("email_session", email);
    }

    @Override
    public Useraccount getCurrentAccount(HttpServletRequest req) {
        HttpSession userSession=null;
        userSession = (HttpSession) req.getSession(false);
        if(userSession!=null){
            String email = (String) userSession.getAttribute("email_session");
            return userAccountService.findByEmail(email);
        }else{
            return null;
        }
    }

    @Override
    public boolean isUserSessionActive(HttpServletRequest req){
        Useraccount acct = getCurrentAccount(req);
        if(acct != null){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void setAnAttribute(HttpServletRequest req, String key, Object value) {
        HttpSession userSession = req.getSession(false);
        if(userSession!= null){
            userSession.setAttribute(key, value);
        }
    }

    @Override
    public Object get(HttpServletRequest req, String key){
        HttpSession userSession=null;
        userSession = (HttpSession) req.getSession(false);
        if(userSession != null)
            return  userSession.getAttribute(key);
        else return null;
    }

    @Override
    public void destroySession(HttpServletRequest req) {
        req.getSession().invalidate();
    }

    @Override
    public boolean isUserAdmin(HttpServletRequest req) {
        HttpSession userSession=null;
        userSession = (HttpSession) req.getSession(false);
        if(userSession!=null){
            String email = (String) userSession.getAttribute("email_session");
            if(email == null)
                return false;


            Useraccount acct =  userAccountService.findByEmail(email);
            if(acct.getRole().getName().equals("ADMINISTRATOR_ROLE"))
                return true;

            return false;

        }else{
            return false;
        }
    }

    @Override
    public boolean isUserPortalUser(HttpServletRequest request) {
        HttpSession userSession=null;
        userSession = (HttpSession) request.getSession(false);
        if(userSession!=null){
            String email = (String) userSession.getAttribute("email_session");
            if(email == null)
                return false;


            Useraccount acct =  userAccountService.findByEmail(email);
            if(acct.getRole().getName().equals("PORTAL_USER_ROLE"))
                return true;

            return false;

        }else{
            return false;
        }
    }
}

package com.rechargenow.demo.service.account;
import com.rechargenow.demo.model.Useraccount;
import javax.servlet.http.HttpServletRequest;


public interface SessionManager
{
    void createSession(HttpServletRequest req, String email);
    Useraccount getCurrentAccount(HttpServletRequest req);
    boolean isUserSessionActive(HttpServletRequest req);
    void setAnAttribute(HttpServletRequest req, String key, Object value);
    Object get(HttpServletRequest req, String key);
    void destroySession(HttpServletRequest req);
    boolean isUserAdmin(HttpServletRequest req);
    boolean isUserPortalUser(HttpServletRequest request);
}

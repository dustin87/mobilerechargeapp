package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.UseraccountDao;
import com.rechargenow.demo.model.Useraccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UseraccountService {
    @Autowired UseraccountDao useraccountDao;

    public Useraccount getOne(Long id) {
        return useraccountDao.getOne(id);
    }

    public Useraccount update(Useraccount account){
        return useraccountDao.save(account);
    }

    public Useraccount save(Useraccount acct)
    {
        return useraccountDao.save(acct);
    }
    public void delete(Useraccount acct){
         useraccountDao.delete(acct);
    }

    public Useraccount findByEmail(String email)
    {
        return useraccountDao.findByEmail(email);
    }
    public Useraccount findBy(String email, String password){
        return useraccountDao.findByEmailAndPassword(email, password);
    }
}

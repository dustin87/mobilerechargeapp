package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.PortalUserDao;
import com.rechargenow.demo.dao.RoleDao;
import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Role;
import com.rechargenow.demo.model.Useraccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortalUserSerivice {
    @Autowired PortalUserDao dao;

    public PortalUser getOne(Long id) {
        return dao.getOne(id);
    }

    public PortalUser update(PortalUser user){
        return dao.save(user);
    }

    public PortalUser save(PortalUser user)
    {
        return dao.save(user);
    }
    public void delete(PortalUser user){
        dao.delete(user);
    }
    public PortalUser findBy(Useraccount useraccount){
        return dao.findByUseraccount(useraccount);
    }

}

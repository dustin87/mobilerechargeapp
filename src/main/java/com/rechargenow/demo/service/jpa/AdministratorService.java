package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.AdministratorDao;
import com.rechargenow.demo.dao.PortalUserDao;
import com.rechargenow.demo.model.Adminstrator;
import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Useraccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministratorService {
    @Autowired AdministratorDao dao;

    public Adminstrator getOne(Long id) {
        return dao.getOne(id);
    }

    public Adminstrator update(Adminstrator admin){
        return dao.save(admin);
    }

    public Adminstrator save(Adminstrator admin)
    {
        return dao.save(admin);
    }
    public void delete(Adminstrator admin){
        dao.delete(admin);
    }

    public Adminstrator findOne(Useraccount useraccount)
    {
        return dao.findByUseraccount(useraccount);
    }

}

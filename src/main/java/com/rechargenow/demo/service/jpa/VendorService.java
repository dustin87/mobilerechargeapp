package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.PortalUserDao;
import com.rechargenow.demo.dao.VendorDao;
import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Vendor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendorService {
    @Autowired VendorDao dao;

    public Vendor getOne(Long id) {
        return dao.getOne(id);
    }

    public Vendor update(Vendor vendor){
        return dao.save(vendor);
    }

    public Vendor save(Vendor vendor)
    {
        return dao.save(vendor);
    }
    public void delete(Vendor vendor){
        dao.delete(vendor);
    }

    public List<Vendor> getAll() {
        return dao.findAll();
    }
    public List<Vendor> findAllBy(String name) {
        return dao.findAllByName(name);
    }
}

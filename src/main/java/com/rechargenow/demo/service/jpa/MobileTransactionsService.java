package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.MobileTransactionDao;
import com.rechargenow.demo.dao.RoleDao;
import com.rechargenow.demo.model.MobileTransactions;
import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MobileTransactionsService {
    @Autowired MobileTransactionDao dao;

    public MobileTransactions getOne(Long id) {
        return dao.getOne(id);
    }

    public MobileTransactions update(MobileTransactions transaction){
        return dao.save(transaction);
    }

    public MobileTransactions save(MobileTransactions transaction)
    {
        return dao.save(transaction);
    }
    public void delete(MobileTransactions transaction){
        dao.delete(transaction);
    }
    public List<MobileTransactions> findAll(){
        return dao.findAll();
    }

    public List<MobileTransactions> findAllBy(PortalUser pUser){
        return dao.findAllByPortalUser(pUser);
    }
}

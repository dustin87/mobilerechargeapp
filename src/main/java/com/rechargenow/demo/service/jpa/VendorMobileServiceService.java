package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.VendorDao;
import com.rechargenow.demo.dao.VendorMobileServiceDao;
import com.rechargenow.demo.model.Vendor;
import com.rechargenow.demo.model.VendorMobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendorMobileServiceService {

    @Autowired VendorMobileServiceDao dao;

    public VendorMobileService getOne(Long id) {
        return dao.getOne(id);
    }

    public VendorMobileService update(VendorMobileService vservice){
        return dao.save(vservice);
    }

    public VendorMobileService save(VendorMobileService vservice)
    {
        return dao.save(vservice);
    }
    public void delete(VendorMobileService vservice){
        dao.delete(vservice);
    }

    public List<VendorMobileService> getAll() {
        return dao.findAll();
    }

    public List<VendorMobileService> findAllBy(Vendor vendor)
    {
        return dao.findAllByVendor(vendor);
    }

    public List<VendorMobileService> findAllBy(String name) {
        return dao.findAllByName(name);
    }
}

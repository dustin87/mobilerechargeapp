package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.TicketDao;
import com.rechargenow.demo.model.PortalUser;
import com.rechargenow.demo.model.Role;
import com.rechargenow.demo.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by David on 7/25/2019.
 */
@Service
public class TicketService {
    @Autowired
    TicketDao dao;

    public Ticket getOne(Long id) {
        return dao.getOne(id);
    }

    public Ticket update(Ticket ticket){
        return dao.save(ticket);
    }

    public Ticket save(Ticket ticket)
    {
        return dao.save(ticket);
    }
    public void delete(Ticket ticket){
        dao.delete(ticket);
    }
    public List<Ticket> findAllBy(PortalUser portalUser){
        return dao.findAllByPortalUser(portalUser);
    }

    public List<Ticket> findAllBy(Ticket.TicketStatus status){
        return dao.findAllByTickeStatus(status);
    }

}

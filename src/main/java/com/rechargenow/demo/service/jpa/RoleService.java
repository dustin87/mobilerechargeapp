package com.rechargenow.demo.service.jpa;

import com.rechargenow.demo.dao.RoleDao;
import com.rechargenow.demo.model.Role;
import com.rechargenow.demo.model.Useraccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    RoleDao roleDao;

    public Role getOne(Long id) {
        return roleDao.getOne(id);
    }

    public Role update(Role role){
        return roleDao.save(role);
    }

    public Role save(Role role)
    {
        return roleDao.save(role);
    }
    public void delete(Role role){
        roleDao.delete(role);
    }

    public Role findByName(String name) {
        return roleDao.findByName(name);
    }
}

package com.rechargenow.demo.config;

import com.rechargenow.demo.model.*;
import com.rechargenow.demo.service.jpa.*;
import com.rechargenow.demo.util.DateTimeUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartUp implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired PortalUserSerivice portalUserSerivice;
    @Autowired RoleService roleService;
    @Autowired DateTimeUtilities dateTimeUtilities;
    @Autowired UseraccountService useraccountService;
    @Autowired AdministratorService administratorService;
    @Autowired VendorService vendorService;
    @Autowired VendorMobileServiceService vendorMobileServiceService;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
    {
        //CREATE NECESSARY ROLES
        if(roleService.findByName("PORTAL_USER_ROLE") == null)
        {
            Role role = new Role();
            role.setName("PORTAL_USER_ROLE");
            roleService.save(role);
        }

        if(roleService.findByName("ADMINISTRATOR_ROLE") == null)
        {
            Role role = new Role();
            role.setName("ADMINISTRATOR_ROLE");
            roleService.save(role);
        }


        //CREATE SUPER USER
        Useraccount adminAcct = useraccountService.findByEmail("admin@mrecharge.com");
        if(adminAcct == null)
        {
            Useraccount acct = new Useraccount();
            acct.setRole(roleService.findByName("ADMINISTRATOR_ROLE"));
            acct.setRegistrationDate(dateTimeUtilities.getDateTime());
            acct.setAccountStatus(Useraccount.AccountStatus.ACTIVE);
            acct.setPassword("password");
            acct.setEmail("admin@mrecharge.com");
            useraccountService.save(acct);

            Adminstrator admin = new Adminstrator();
            admin.setFirstName("Tom");
            admin.setLastName("Hanks");
            admin.setClearanceLevel("HIGHEST");
            admin.setDepartment("IT");
            admin.setUseraccount(acct);
            administratorService.save(admin);
        }


        //CREATE A DEFAULT VENDOR
        if(vendorService.findAllBy("MTN").isEmpty()){
            Vendor vendor = new Vendor();
            vendor.setName("MTN");
            vendor.setStatus(Vendor.AccountStatus.ACTIVE);
            vendor.setAdminstrator(administratorService.findOne(adminAcct));
            vendor.setRegistrationDate(dateTimeUtilities.getDateTime());
            vendor = vendorService.save(vendor);

            VendorMobileService vm = new VendorMobileService();
            vm.setVendor(vendor);
            vm.setAmount(2000.0);
            vm.setName("MTN XTRA COOL");
            vendorMobileServiceService.save(vm);
        }


    }
}
